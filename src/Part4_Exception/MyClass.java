package Part4_Exception;

public class MyClass {
	
	// checked exception
	public void methX() throws DataException{ 
		throw new DataException();
	}
	
	// unchecked exception
	public void methY(){
		throw new FormatException();
	}
}
