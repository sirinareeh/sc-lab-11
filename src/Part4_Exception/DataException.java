package Part4_Exception;

public class DataException extends Exception{
	public DataException(){
		super();
	}
	public DataException(String message){
		super(message);
	}
}
