package Part5_Exception;

public class FullException extends Exception{
	public FullException(){
		super();
	}
	public FullException(String message){
		super(message);
	}
}
