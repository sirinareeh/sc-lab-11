package Part5_Exception;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Refrigerator ref = new Refrigerator(4);
		System.out.print(ref.toString());
		try {
			ref.put("Pizza");
			ref.put("Chocolate");
			ref.put("Corn Soup");
			ref.put("Milk");
			
			ref.put("Beer"); 
		} catch (FullException e) {
			System.err.println("Refrigerator is already full !!");
		} finally {
			ref.takeOut("Beer");
			System.out.println("\n"+ref.toString());
		}
	}
}
