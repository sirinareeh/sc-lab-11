package Part3_WordCounter;

import java.util.HashMap;

public class WordCounter {
	private String message;
	private HashMap<String,Integer> wordCount;
	
	public WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String,Integer>();
	}
	public void count(){
		String[] list = message.split(" ");
		for (String i : list){
			if(wordCount.containsKey(i))
				wordCount.put(i, wordCount.get(i)+1);
			else
				wordCount.put(i, 1);
		}
	}
	
	public int hasWord(String word){
		if(wordCount.containsKey(word))
			return wordCount.get(word);
		else
			return 0;
		
		
	}
}
